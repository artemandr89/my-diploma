import Vue from 'vue'
import Vuelidate from 'vuelidate'
import Paginate from 'vuejs-paginate'
import App from './App.vue'
import router from './router'
import store from './store/index'
import Loader from './components/app/Loader'
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'
import currencyFilter from './filters/currency.filter'
import dateFilter from './filters/date.filter'
import tooltipDirective from './directives/tooltip.directive'
import MessagePlugin from './message-plugin'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false

Vue.use(MessagePlugin)
Vue.use(Vuelidate)

Vue.component('paginate', Paginate)
Vue.component('loader', Loader)
Vue.filter('currency', currencyFilter)
Vue.filter('date', dateFilter)
Vue.directive('tooltip', tooltipDirective)

firebase.initializeApp({
  apiKey: "AIzaSyAIcjCn6Hkty1iVLLtee0cG9VGY5TDFZE4",
  authDomain: "diploma-vue.firebaseapp.com",
  databaseURL: "https://diploma-vue.firebaseio.com",
  projectId: "diploma-vue",
  storageBucket: "diploma-vue.appspot.com",
  messagingSenderId: "1057794886269",
  appId: "1:1057794886269:web:0bd93ea45557f62696ff14",
  measurementId: "G-6D6WSTD43X"
})

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
